package main.controller;

import javafx.application.Platform;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.event.ActionEvent;
import javafx.event.EventHandler;
import javafx.fxml.FXML;
import javafx.geometry.Insets;
import javafx.geometry.Pos;
import javafx.scene.Cursor;
import javafx.scene.control.*;
import javafx.scene.control.cell.PropertyValueFactory;
import javafx.scene.input.MouseButton;
import javafx.scene.input.MouseEvent;
import javafx.scene.layout.BorderPane;
import javafx.scene.layout.GridPane;
import javafx.scene.layout.HBox;
import javafx.scene.layout.VBox;
import main.components.dialogs.ErrorDialog;
import main.components.fields.NumericTextField;
import main.events.SaveToFile;
import main.exceptions.IlegalTimeAccessException;
import main.exceptions.UnauthorizedAccessException;
import main.presistence.UserDetailsEntity;
import main.utils.JpaDatabaseUtil;
import main.utils.JpaProcedureManager;
import main.utils.TimerBannedUser;
import org.hibernate.exception.JDBCConnectionException;
import org.hibernate.exception.SQLGrammarException;

import java.sql.Date;
import java.time.LocalDate;
import java.util.Collections;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;


public class Controller {
    private static final String CONTEXT = "context";
    private static final Logger log = Logger.getLogger(Controller.class.getName());

    private BorderPane root;
    @FXML
    public VBox vBox;
    @FXML
    public TableView<UserDetailsEntity> tableView;
    @FXML
    private HBox hBox;
    private TextField textField;
    private TextField secondTextField;
    private NumericTextField numericTextField;
    private DatePicker datePickerFrom;
    private DatePicker datePickerTo;
    private ComboBox<String> comboBox;
    private GridPane gridPane;
    private Label labelField;

    public TableColumn<UserDetailsEntity, String> userId;
    public TableColumn<UserDetailsEntity, String> username;
    public TableColumn<UserDetailsEntity, String> firstname;
    public TableColumn<UserDetailsEntity, String> lastname;
    public TableColumn<UserDetailsEntity, String> gender;
    public TableColumn<UserDetailsEntity, String> city;
    public TableColumn<UserDetailsEntity, String> street;
    public TableColumn<UserDetailsEntity, String> number;
    public TableColumn<UserDetailsEntity, String> email;
    public TableColumn<UserDetailsEntity, String> mobilePhone;
    public TableColumn<UserDetailsEntity, String> phoneNumber;

    private List<UserDetailsEntity> resultUserDetailsEntityList;
    private ObservableList<UserDetailsEntity> data;
    public int indexMenuItem;
    private int elementsToDelete;

    private Button btSaveToFile;
    private Button btSearch;
    private TimerBannedUser timerBannedUser;

    public void setRoot(BorderPane root) {
        this.root = root;
        hBox = (HBox) this.root.getBottom();
        timerBannedUser = new TimerBannedUser((Label) hBox.getChildren().get(0));
    }

    public Button getBtSaveToFile() {
        return btSaveToFile;
    }

    public List<UserDetailsEntity> getResultUserDetailsEntityList() {
        return resultUserDetailsEntityList;
    }

    public ObservableList<UserDetailsEntity> getData() {
        return data;
    }

    private EventHandler<MouseEvent> onClickSearch = new EventHandler<MouseEvent>() {
        @Override
        public void handle(MouseEvent event) {
            if (event.getButton().equals(MouseButton.PRIMARY) && validationFields()) {
                if (!timerBannedUser.isBanned() && event.getClickCount() > 5) {
                    new ErrorDialog("Banned", "Zostałeś zablokowany przez mouse spamming click. Informacja o czasie blokady jest w stopce", Alert.AlertType.ERROR).show();
                    timerBannedUser.start();
                } else if (!timerBannedUser.isBanned()) {
                    btSearch.setDisable(true);
                    root.setCursor(Cursor.WAIT);
                    callStoredProcedureThread().start();
                }
            } else {
                String title = "Validation error";
                String message;
                if (indexMenuItem == 6) {
                    message = "Podałeś nie poprawny format numeru telefonu (XXX-XXX-XXX)";
                } else {
                    message = "Pole nie może być puste";
                }
                showDialog(title, message, Alert.AlertType.ERROR);
            }
        }

        private Thread callStoredProcedureThread() {
            return new Thread(() -> {
                try {
                    resultUserDetailsEntityList = callStoredProcedures();
                    if (resultUserDetailsEntityList != null) {
                        if (!resultUserDetailsEntityList.isEmpty()) {
                            btSaveToFile.setDisable(false);
                        } else {
                            btSaveToFile.setDisable(true);
                            showDialog("Brak danych", "Brak danych o podanych kryteriach", Alert.AlertType.INFORMATION);
                        }
                        data = FXCollections.observableArrayList();
                        data.addAll(resultUserDetailsEntityList);
                        tableView.setItems(data);
                    }
                } catch (IllegalAccessError ex) {
                    log.log(Level.WARNING, CONTEXT, ex);
                    showDialog("Communications link failure", "Connection refused or connection time out. Check Internet Connection", Alert.AlertType.WARNING);
                } catch (IlegalTimeAccessException ex) {
                    log.log(Level.WARNING, CONTEXT, ex);
                    showDialog("Disabled access", "Prevent access to database from 00:00 to 07:00", Alert.AlertType.INFORMATION);
                } catch(UnauthorizedAccessException ex){
                    log.log(Level.WARNING, CONTEXT, ex);
                    showDialog("Unauthorized access", "You are connecting by unauthorized application.", Alert.AlertType.WARNING);
                } finally {
                    unlockSearchButtonAndDisableWaitCursor();
                }
            });
        }

        private boolean validationFields() {
            if (gridPane.getChildren().contains(textField)) {
                if (textField.getText().isEmpty()) {
                    return false;
                } else if (indexMenuItem == 6 && !textField.getText().matches("^\\d{3}\\-\\d{3}\\-\\d{3}$")) {
                    return false;
                }
            }
            if (gridPane.getChildren().contains(secondTextField) && secondTextField.getText().isEmpty()) {
                return false;
            }
            if (gridPane.getChildren().contains(numericTextField) && numericTextField.getText().isEmpty()) {
                return false;
            }
            return true;
        }
    };

    @FXML
    public void exitApplication(ActionEvent event) {
        JpaDatabaseUtil.closeEntityManagerFactory();
        Platform.exit();
    }

    @FXML
    public void initialize() {
        textField = new TextField();
        secondTextField = new TextField();
        numericTextField = new NumericTextField();
        datePickerFrom = new DatePicker();
        datePickerTo = new DatePicker();
        comboBox = new ComboBox();
        labelField = new Label();
        btSaveToFile = new Button("Save to file");
        btSearch = new Button("Search");
        btSearch.setOnMouseClicked(onClickSearch);
        btSaveToFile.setOnAction(new SaveToFile());
        userId.setCellValueFactory(new PropertyValueFactory<UserDetailsEntity, String>("userId"));
        username.setCellValueFactory(new PropertyValueFactory<UserDetailsEntity, String>("username"));
        firstname.setCellValueFactory(new PropertyValueFactory<UserDetailsEntity, String>("firstName"));
        lastname.setCellValueFactory(new PropertyValueFactory<UserDetailsEntity, String>("lastName"));
        gender.setCellValueFactory(new PropertyValueFactory<UserDetailsEntity, String>("gender"));
        city.setCellValueFactory(new PropertyValueFactory<UserDetailsEntity, String>("city"));
        street.setCellValueFactory(new PropertyValueFactory<UserDetailsEntity, String>("street"));
        number.setCellValueFactory(new PropertyValueFactory<UserDetailsEntity, String>("number"));
        email.setCellValueFactory(new PropertyValueFactory<UserDetailsEntity, String>("email"));
        mobilePhone.setCellValueFactory(new PropertyValueFactory<UserDetailsEntity, String>("mobileNumber"));
        phoneNumber.setCellValueFactory(new PropertyValueFactory<UserDetailsEntity, String>("phoneNumber"));
        indexMenuItem = 0;
        elementsToDelete = 0;
        initGridPane();
    }

    public void initGridPane() {
        gridPane = new GridPane();
        gridPane.setPrefHeight(50);
        gridPane.add(labelField, 0, 0, 1, 2);
        labelField.setText("Enter username:");
        gridPane.add(btSearch, 3, 0, 1, 2);
        gridPane.add(btSaveToFile, 4, 0, 1, 2);
        gridPane.setAlignment(Pos.BASELINE_CENTER);
        gridPane.setHgap(10);
        gridPane.setVgap(5);
        gridPane.setPadding(new Insets(10, 0, 10, 0));
        vBox.getChildren().add(0, gridPane);
        setOneFieldTextSearching();
    }

    public void setOneFieldTextSearching() {
        clearData();
        gridPane.getChildren().remove(gridPane.getChildren().size() - elementsToDelete, gridPane.getChildren().size());
        elementsToDelete = 1;
        gridPane.add(textField, 1, 0);
    }

    public void setOneFieldNumericTextSearching() {
        clearData();
        gridPane.getChildren().remove(gridPane.getChildren().size() - elementsToDelete, gridPane.getChildren().size());
        elementsToDelete = 1;
        gridPane.add(numericTextField, 1, 0);
    }

    public void setOneFieldComboSearching(ObservableList<String> observableList) {
        clearData();
        gridPane.getChildren().remove(gridPane.getChildren().size() - elementsToDelete, gridPane.getChildren().size());
        elementsToDelete = 1;
        comboBox.getItems().setAll(observableList);
        comboBox.getSelectionModel().select(0);
        gridPane.add(comboBox, 1, 0);
    }

    public void setOneFieldDataSearching() {
        clearData();
        gridPane.getChildren().remove(gridPane.getChildren().size() - elementsToDelete, gridPane.getChildren().size());
        elementsToDelete = 1;
        datePickerFrom.setValue(LocalDate.now());
        datePickerFrom.getEditor().setText(LocalDate.now().toString());
        gridPane.add(datePickerFrom, 1, 0);
    }

    public void setTwoFieldTextSearching() {
        clearData();
        gridPane.getChildren().remove(gridPane.getChildren().size() - elementsToDelete, gridPane.getChildren().size());
        elementsToDelete = 4;
        gridPane.add(new Label("City:"), 1, 0);
        gridPane.add(new Label("Street:"), 1, 1);
        gridPane.add(textField, 2, 0);
        gridPane.add(secondTextField, 2, 1);
    }

    public void setTwoFieldDataSearching() {
        clearData();
        gridPane.getChildren().remove(gridPane.getChildren().size() - elementsToDelete, gridPane.getChildren().size());
        elementsToDelete = 4;
        gridPane.add(new Label("Od:"), 1, 0);
        gridPane.add(new Label("Do:"), 1, 1);
        datePickerFrom.setValue(LocalDate.now());
        datePickerFrom.getEditor().setText(LocalDate.now().toString());
        datePickerTo.setValue(LocalDate.now());
        datePickerTo.getEditor().setText(LocalDate.now().toString());
        gridPane.add(datePickerFrom, 2, 0);
        gridPane.add(datePickerTo, 2, 1);
    }

    public void setLabelFieldText(String text) {
        labelField.setText(text);
    }

    public void unlockSearchButtonAndDisableWaitCursor() {
        root.setCursor(Cursor.DEFAULT);
        btSearch.setDisable(false);
    }

    public void showDialog(String title, String text, Alert.AlertType alertType) {
        Platform.runLater(() -> new ErrorDialog(title, text, alertType).showAndWait());
    }

    public void clearData() {
        btSaveToFile.setDisable(true);
        tableView.getItems().clear();
        textField.clear();
        secondTextField.clear();
        datePickerFrom.getEditor().clear();
        datePickerTo.getEditor().clear();
        numericTextField.clear();
    }


    private List<UserDetailsEntity> callStoredProcedures() {
        try {
            switch (indexMenuItem) {
                case 0:
                    return JpaProcedureManager.findUser(textField.getText());
                case 1:
                    return JpaProcedureManager.findUsersFromGivenAddress(textField.getText(), secondTextField.getText());
                case 2:
                    return JpaProcedureManager.findAccountsOlderFromDate(Date.valueOf(datePickerFrom.getValue()));
                case 3:
                    return JpaProcedureManager.findUserActiveFromGivenCountOfMonths(numericTextField.getText());
                case 4:
                    return JpaProcedureManager.findUsersByGender(comboBox.getSelectionModel().getSelectedItem());
                case 5:
                    return JpaProcedureManager.findUsersByStatus("Active".equals(comboBox.getSelectionModel().getSelectedItem()) ? 1 : 0);
                case 6:
                    return JpaProcedureManager.findUserByMobilePhoneNumber(textField.getText());
                case 7:
                    return JpaProcedureManager.findUserLogginIngivenPeriodOfTime(Date.valueOf(datePickerFrom.getValue()), Date.valueOf(datePickerTo.getValue()));
                case 8:
                    return JpaProcedureManager.findUsersByServiceOfWebMail(comboBox.getSelectionModel().getSelectedItem());
                case 9:
                    return JpaProcedureManager.showUsersUsedGivenMobileOperator(comboBox.getSelectionModel().getSelectedItem());
                default:
                    return Collections.emptyList();
            }
        } catch (javax.persistence.PersistenceException ex) {
            if (ex.getCause() instanceof JDBCConnectionException) {
                log.log(Level.WARNING, CONTEXT, ex);
                JpaDatabaseUtil.resetEntityManagerFactory();
                throw new IllegalAccessError();
            }
            if (ex.getCause() instanceof SQLGrammarException) {
                log.log(Level.WARNING, CONTEXT, ex);
                throw new IlegalTimeAccessException();
            }
        }
        throw new UnauthorizedAccessException();
    }
}
