package main.utils;

import javafx.scene.control.Alert;
import main.Main;
import main.events.SaveToFile;

import javax.persistence.EntityManager;
import javax.persistence.EntityManagerFactory;
import javax.persistence.Persistence;
import java.util.logging.Level;
import java.util.logging.Logger;


/**
 * Created by Mateusz Dobrowolski on 2016-11-12.
 */
public class JpaDatabaseUtil {
    private static final String CONTEXT = "context";
    private static final Logger log = Logger.getLogger(SaveToFile.class.getName());
    private static final String PERISTENCE_UNIT_NAME = "isdb";
    private static EntityManagerFactory entityManagerFactory;
    private static EntityManager entityManager;

    public static EntityManager getEntityManager() {
        if (entityManagerFactory == null) {
            try {
                entityManagerFactory = Persistence.createEntityManagerFactory(PERISTENCE_UNIT_NAME);
                entityManager = entityManagerFactory.createEntityManager();
            } catch (Throwable ex) {
                log.log(Level.WARNING, CONTEXT, ex);
                Main.controller.showDialog("Communications link failure", "Connection refused or connection timed out. Check Internet connection or rules of firewall.", Alert.AlertType.WARNING);
                Main.controller.unlockSearchButtonAndDisableWaitCursor();
                throw new ExceptionInInitializerError(ex);
            }
        }
        return entityManager;
    }

    public static void resetEntityManagerFactory() {
        entityManagerFactory = null;
    }

    public static void closeEntityManagerFactory() {
        entityManagerFactory.close();
    }
}
