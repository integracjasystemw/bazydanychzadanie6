package main.utils;

import main.presistence.UserDetailsEntity;

import javax.persistence.*;
import java.sql.Date;
import java.util.List;

/**
 * Created by Mateusz Dobrowolski on 2016-11-12.
 */
public class JpaProcedureManager {

    private JpaProcedureManager() {
    }

    public static List<UserDetailsEntity> findAccountsOlderFromDate(Date date) throws PersistenceException {
        int successStatus;
        EntityManager em = JpaDatabaseUtil.getEntityManager();
        EntityTransaction et = em.getTransaction();
        rollBackIfTransactionIsActive(et);
        StoredProcedureQuery query = em.createStoredProcedureQuery("findAccountsOlderThan", UserDetailsEntity.class)
                .registerStoredProcedureParameter(0, Date.class, ParameterMode.IN)
                .registerStoredProcedureParameter(1, String.class, ParameterMode.IN)
                .registerStoredProcedureParameter(2, String.class, ParameterMode.IN)
                .registerStoredProcedureParameter(3, String.class, ParameterMode.IN)
                .registerStoredProcedureParameter(4, String.class, ParameterMode.IN)
                .registerStoredProcedureParameter(5, String.class, ParameterMode.IN)
                .registerStoredProcedureParameter(6, String.class, ParameterMode.IN)
                .registerStoredProcedureParameter(7, String.class, ParameterMode.IN)
                .registerStoredProcedureParameter(8, Integer.class, ParameterMode.OUT);
        query.setParameter(0, date)
                .setParameter(1, ComputerDataInstance.getComputerData().getHashMd5())
                .setParameter(2, ComputerDataInstance.getComputerData().getIp())
                .setParameter(3, ComputerDataInstance.getComputerData().getComputerName())
                .setParameter(4, ComputerDataInstance.getComputerData().getNameOs())
                .setParameter(5, ComputerDataInstance.getComputerData().getVersionOs())
                .setParameter(6, ComputerDataInstance.getComputerData().getArchitectureOS())
                .setParameter(7, ComputerDataInstance.getComputerData().getAccountName());
        et.begin();
        query.execute();
        et.commit();
        successStatus = (int) query.getOutputParameterValue(8);
        return successStatus == 1 ? (List<UserDetailsEntity>) query.getResultList() : null;
    }

    public static List<UserDetailsEntity> findUserActiveFromGivenCountOfMonths(String countOfMonths) throws PersistenceException {
        int successStatus;
        EntityManager em = JpaDatabaseUtil.getEntityManager();
        EntityTransaction et = em.getTransaction();
        rollBackIfTransactionIsActive(et);
        StoredProcedureQuery query = em.createStoredProcedureQuery("findUserActiveFromGivenCountOfMonths", UserDetailsEntity.class)
                .registerStoredProcedureParameter(0, String.class, ParameterMode.IN)
                .registerStoredProcedureParameter(1, String.class, ParameterMode.IN)
                .registerStoredProcedureParameter(2, String.class, ParameterMode.IN)
                .registerStoredProcedureParameter(3, String.class, ParameterMode.IN)
                .registerStoredProcedureParameter(4, String.class, ParameterMode.IN)
                .registerStoredProcedureParameter(5, String.class, ParameterMode.IN)
                .registerStoredProcedureParameter(6, String.class, ParameterMode.IN)
                .registerStoredProcedureParameter(7, String.class, ParameterMode.IN)
                .registerStoredProcedureParameter(8, Integer.class, ParameterMode.OUT);
        query.setParameter(0, countOfMonths)
                .setParameter(1, ComputerDataInstance.getComputerData().getHashMd5())
                .setParameter(2, ComputerDataInstance.getComputerData().getIp())
                .setParameter(3, ComputerDataInstance.getComputerData().getComputerName())
                .setParameter(4, ComputerDataInstance.getComputerData().getNameOs())
                .setParameter(5, ComputerDataInstance.getComputerData().getVersionOs())
                .setParameter(6, ComputerDataInstance.getComputerData().getArchitectureOS())
                .setParameter(7, ComputerDataInstance.getComputerData().getAccountName());
        et.begin();
        query.execute();
        et.commit();
        successStatus = (int) query.getOutputParameterValue(8);
        return successStatus == 1 ? (List<UserDetailsEntity>) query.getResultList() : null;
    }

    public static List<UserDetailsEntity> findUser(String userName) throws PersistenceException {
        int successStatus;
        EntityManager em = JpaDatabaseUtil.getEntityManager();
        EntityTransaction et = em.getTransaction();
        rollBackIfTransactionIsActive(et);
        StoredProcedureQuery query = em.createStoredProcedureQuery("findUser", UserDetailsEntity.class)
                .registerStoredProcedureParameter(0, String.class, ParameterMode.IN)
                .registerStoredProcedureParameter(1, String.class, ParameterMode.IN)
                .registerStoredProcedureParameter(2, String.class, ParameterMode.IN)
                .registerStoredProcedureParameter(3, String.class, ParameterMode.IN)
                .registerStoredProcedureParameter(4, String.class, ParameterMode.IN)
                .registerStoredProcedureParameter(5, String.class, ParameterMode.IN)
                .registerStoredProcedureParameter(6, String.class, ParameterMode.IN)
                .registerStoredProcedureParameter(7, String.class, ParameterMode.IN)
                .registerStoredProcedureParameter(8, Integer.class, ParameterMode.OUT);
        query.setParameter(0, userName)
                .setParameter(1, ComputerDataInstance.getComputerData().getHashMd5())
                .setParameter(2, ComputerDataInstance.getComputerData().getIp())
                .setParameter(3, ComputerDataInstance.getComputerData().getComputerName())
                .setParameter(4, ComputerDataInstance.getComputerData().getNameOs())
                .setParameter(5, ComputerDataInstance.getComputerData().getVersionOs())
                .setParameter(6, ComputerDataInstance.getComputerData().getArchitectureOS())
                .setParameter(7, ComputerDataInstance.getComputerData().getAccountName());
        et.begin();
        query.execute();
        et.commit();
        successStatus = (int) query.getOutputParameterValue(8);
        return successStatus == 1 ? (List<UserDetailsEntity>) query.getResultList() : null;
    }

    public static List<UserDetailsEntity> findUsersByGender(String gender) throws PersistenceException {
        int successStatus;
        EntityManager em = JpaDatabaseUtil.getEntityManager();
        EntityTransaction et = em.getTransaction();
        rollBackIfTransactionIsActive(et);
        StoredProcedureQuery query = em.createStoredProcedureQuery("findPersonByGender", UserDetailsEntity.class)
                .registerStoredProcedureParameter(0, String.class, ParameterMode.IN)
                .registerStoredProcedureParameter(1, String.class, ParameterMode.IN)
                .registerStoredProcedureParameter(2, String.class, ParameterMode.IN)
                .registerStoredProcedureParameter(3, String.class, ParameterMode.IN)
                .registerStoredProcedureParameter(4, String.class, ParameterMode.IN)
                .registerStoredProcedureParameter(5, String.class, ParameterMode.IN)
                .registerStoredProcedureParameter(6, String.class, ParameterMode.IN)
                .registerStoredProcedureParameter(7, String.class, ParameterMode.IN)
                .registerStoredProcedureParameter(8, Integer.class, ParameterMode.OUT);
        query.setParameter(0, gender)
                .setParameter(1, ComputerDataInstance.getComputerData().getHashMd5())
                .setParameter(2, ComputerDataInstance.getComputerData().getIp())
                .setParameter(3, ComputerDataInstance.getComputerData().getComputerName())
                .setParameter(4, ComputerDataInstance.getComputerData().getNameOs())
                .setParameter(5, ComputerDataInstance.getComputerData().getVersionOs())
                .setParameter(6, ComputerDataInstance.getComputerData().getArchitectureOS())
                .setParameter(7, ComputerDataInstance.getComputerData().getAccountName());
        et.begin();
        query.execute();
        et.commit();
        successStatus = (int) query.getOutputParameterValue(8);
        return successStatus == 1 ? (List<UserDetailsEntity>) query.getResultList() : null;
    }

    public static List<UserDetailsEntity> findUserByMobilePhoneNumber(String numberMobilePhone) throws PersistenceException {
        int successStatus;
        EntityManager em = JpaDatabaseUtil.getEntityManager();
        EntityTransaction et = em.getTransaction();
        rollBackIfTransactionIsActive(et);
        StoredProcedureQuery query = em.createStoredProcedureQuery("findUserByMobilePhone", UserDetailsEntity.class)
                .registerStoredProcedureParameter(0, String.class, ParameterMode.IN)
                .registerStoredProcedureParameter(1, String.class, ParameterMode.IN)
                .registerStoredProcedureParameter(2, String.class, ParameterMode.IN)
                .registerStoredProcedureParameter(3, String.class, ParameterMode.IN)
                .registerStoredProcedureParameter(4, String.class, ParameterMode.IN)
                .registerStoredProcedureParameter(5, String.class, ParameterMode.IN)
                .registerStoredProcedureParameter(6, String.class, ParameterMode.IN)
                .registerStoredProcedureParameter(7, String.class, ParameterMode.IN)
                .registerStoredProcedureParameter(8, Integer.class, ParameterMode.OUT);
        query.setParameter(0, numberMobilePhone)
                .setParameter(1, ComputerDataInstance.getComputerData().getHashMd5())
                .setParameter(2, ComputerDataInstance.getComputerData().getIp())
                .setParameter(3, ComputerDataInstance.getComputerData().getComputerName())
                .setParameter(4, ComputerDataInstance.getComputerData().getNameOs())
                .setParameter(5, ComputerDataInstance.getComputerData().getVersionOs())
                .setParameter(6, ComputerDataInstance.getComputerData().getArchitectureOS())
                .setParameter(7, ComputerDataInstance.getComputerData().getAccountName());
        et.begin();
        query.execute();
        et.commit();
        successStatus = (int) query.getOutputParameterValue(8);
        return successStatus == 1 ? (List<UserDetailsEntity>) query.getResultList() : null;
    }

    public static List<UserDetailsEntity> showUsersUsedGivenMobileOperator(String mobileOperator) throws PersistenceException {
        int successStatus;
        EntityManager em = JpaDatabaseUtil.getEntityManager();
        EntityTransaction et = em.getTransaction();
        rollBackIfTransactionIsActive(et);
        StoredProcedureQuery query = em.createStoredProcedureQuery("showUsersUsedGivenMobileOperator", UserDetailsEntity.class)
                .registerStoredProcedureParameter(0, String.class, ParameterMode.IN)
                .registerStoredProcedureParameter(1, String.class, ParameterMode.IN)
                .registerStoredProcedureParameter(2, String.class, ParameterMode.IN)
                .registerStoredProcedureParameter(3, String.class, ParameterMode.IN)
                .registerStoredProcedureParameter(4, String.class, ParameterMode.IN)
                .registerStoredProcedureParameter(5, String.class, ParameterMode.IN)
                .registerStoredProcedureParameter(6, String.class, ParameterMode.IN)
                .registerStoredProcedureParameter(7, String.class, ParameterMode.IN)
                .registerStoredProcedureParameter(8, Integer.class, ParameterMode.OUT);
        query.setParameter(0, mobileOperator)
                .setParameter(1, ComputerDataInstance.getComputerData().getHashMd5())
                .setParameter(2, ComputerDataInstance.getComputerData().getIp())
                .setParameter(3, ComputerDataInstance.getComputerData().getComputerName())
                .setParameter(4, ComputerDataInstance.getComputerData().getNameOs())
                .setParameter(5, ComputerDataInstance.getComputerData().getVersionOs())
                .setParameter(6, ComputerDataInstance.getComputerData().getArchitectureOS())
                .setParameter(7, ComputerDataInstance.getComputerData().getAccountName());
        et.begin();
        query.execute();
        et.commit();
        successStatus = (int) query.getOutputParameterValue(8);
        return successStatus == 1 ? (List<UserDetailsEntity>) query.getResultList() : null;
    }

    public static List<UserDetailsEntity> findUsersByServiceOfWebMail(String webMail) throws PersistenceException {
        int successStatus;
        EntityManager em = JpaDatabaseUtil.getEntityManager();
        EntityTransaction et = em.getTransaction();
        rollBackIfTransactionIsActive(et);
        StoredProcedureQuery query = em.createStoredProcedureQuery("findUsersByServiceOfWebMail", UserDetailsEntity.class)
                .registerStoredProcedureParameter(0, String.class, ParameterMode.IN)
                .registerStoredProcedureParameter(1, String.class, ParameterMode.IN)
                .registerStoredProcedureParameter(2, String.class, ParameterMode.IN)
                .registerStoredProcedureParameter(3, String.class, ParameterMode.IN)
                .registerStoredProcedureParameter(4, String.class, ParameterMode.IN)
                .registerStoredProcedureParameter(5, String.class, ParameterMode.IN)
                .registerStoredProcedureParameter(6, String.class, ParameterMode.IN)
                .registerStoredProcedureParameter(7, String.class, ParameterMode.IN)
                .registerStoredProcedureParameter(8, Integer.class, ParameterMode.OUT);
        query.setParameter(0, webMail)
                .setParameter(1, ComputerDataInstance.getComputerData().getHashMd5())
                .setParameter(2, ComputerDataInstance.getComputerData().getIp())
                .setParameter(3, ComputerDataInstance.getComputerData().getComputerName())
                .setParameter(4, ComputerDataInstance.getComputerData().getNameOs())
                .setParameter(5, ComputerDataInstance.getComputerData().getVersionOs())
                .setParameter(6, ComputerDataInstance.getComputerData().getArchitectureOS())
                .setParameter(7, ComputerDataInstance.getComputerData().getAccountName());
        et.begin();
        query.execute();
        et.commit();
        successStatus = (int) query.getOutputParameterValue(8);
        return successStatus == 1 ? (List<UserDetailsEntity>) query.getResultList() : null;
    }

    public static List<UserDetailsEntity> findUsersByStatus(int status) throws PersistenceException {
        int successStatus;
        EntityManager em = JpaDatabaseUtil.getEntityManager();
        EntityTransaction et = em.getTransaction();
        rollBackIfTransactionIsActive(et);
        StoredProcedureQuery query = em.createStoredProcedureQuery("findUsersByStatus", UserDetailsEntity.class)
                .registerStoredProcedureParameter(0, Integer.class, ParameterMode.IN)
                .registerStoredProcedureParameter(1, String.class, ParameterMode.IN)
                .registerStoredProcedureParameter(2, String.class, ParameterMode.IN)
                .registerStoredProcedureParameter(3, String.class, ParameterMode.IN)
                .registerStoredProcedureParameter(4, String.class, ParameterMode.IN)
                .registerStoredProcedureParameter(5, String.class, ParameterMode.IN)
                .registerStoredProcedureParameter(6, String.class, ParameterMode.IN)
                .registerStoredProcedureParameter(7, String.class, ParameterMode.IN)
                .registerStoredProcedureParameter(8, Integer.class, ParameterMode.OUT);
        query.setParameter(0, status)
                .setParameter(1, ComputerDataInstance.getComputerData().getHashMd5())
                .setParameter(2, ComputerDataInstance.getComputerData().getIp())
                .setParameter(3, ComputerDataInstance.getComputerData().getComputerName())
                .setParameter(4, ComputerDataInstance.getComputerData().getNameOs())
                .setParameter(5, ComputerDataInstance.getComputerData().getVersionOs())
                .setParameter(6, ComputerDataInstance.getComputerData().getArchitectureOS())
                .setParameter(7, ComputerDataInstance.getComputerData().getAccountName());
        et.begin();
        query.execute();
        et.commit();
        successStatus = (int) query.getOutputParameterValue(8);
        return successStatus == 1 ? (List<UserDetailsEntity>) query.getResultList() : null;
    }

    public static List<UserDetailsEntity> findUsersFromGivenAddress(String city, String street) throws PersistenceException {
        int successStatus;
        EntityManager em = JpaDatabaseUtil.getEntityManager();
        EntityTransaction et = em.getTransaction();
        rollBackIfTransactionIsActive(et);
        StoredProcedureQuery query = em.createStoredProcedureQuery("findUsersFromGivenAddress", UserDetailsEntity.class)
                .registerStoredProcedureParameter(0, String.class, ParameterMode.IN)
                .registerStoredProcedureParameter(1, String.class, ParameterMode.IN)
                .registerStoredProcedureParameter(2, String.class, ParameterMode.IN)
                .registerStoredProcedureParameter(3, String.class, ParameterMode.IN)
                .registerStoredProcedureParameter(4, String.class, ParameterMode.IN)
                .registerStoredProcedureParameter(5, String.class, ParameterMode.IN)
                .registerStoredProcedureParameter(6, String.class, ParameterMode.IN)
                .registerStoredProcedureParameter(7, String.class, ParameterMode.IN)
                .registerStoredProcedureParameter(8, String.class, ParameterMode.IN)
                .registerStoredProcedureParameter(9, Integer.class, ParameterMode.OUT);
        query.setParameter(0, city).
                setParameter(1, street)
                .setParameter(2, ComputerDataInstance.getComputerData().getHashMd5())
                .setParameter(3, ComputerDataInstance.getComputerData().getIp())
                .setParameter(4, ComputerDataInstance.getComputerData().getComputerName())
                .setParameter(5, ComputerDataInstance.getComputerData().getNameOs())
                .setParameter(6, ComputerDataInstance.getComputerData().getVersionOs())
                .setParameter(7, ComputerDataInstance.getComputerData().getArchitectureOS())
                .setParameter(8, ComputerDataInstance.getComputerData().getAccountName());
        et.begin();
        query.execute();
        et.commit();
        successStatus = (int) query.getOutputParameterValue(9);
        return successStatus == 1 ? (List<UserDetailsEntity>) query.getResultList() : null;
    }

    public static List<UserDetailsEntity> findUserLogginIngivenPeriodOfTime(Date dateFrom, Date dateTo) throws PersistenceException {
        int successStatus;
        EntityManager em = JpaDatabaseUtil.getEntityManager();
        EntityTransaction et = em.getTransaction();
        rollBackIfTransactionIsActive(et);
        StoredProcedureQuery query = em.createStoredProcedureQuery("findUserLogginIngivenPeriodOfTime", UserDetailsEntity.class)
                .registerStoredProcedureParameter(0, Date.class, ParameterMode.IN)
                .registerStoredProcedureParameter(1, Date.class, ParameterMode.IN)
                .registerStoredProcedureParameter(2, String.class, ParameterMode.IN)
                .registerStoredProcedureParameter(3, String.class, ParameterMode.IN)
                .registerStoredProcedureParameter(4, String.class, ParameterMode.IN)
                .registerStoredProcedureParameter(5, String.class, ParameterMode.IN)
                .registerStoredProcedureParameter(6, String.class, ParameterMode.IN)
                .registerStoredProcedureParameter(7, String.class, ParameterMode.IN)
                .registerStoredProcedureParameter(8, String.class, ParameterMode.IN)
                .registerStoredProcedureParameter(9, Integer.class, ParameterMode.OUT);
        query.setParameter(0, dateFrom).
                setParameter(1, dateTo)
                .setParameter(2, ComputerDataInstance.getComputerData().getHashMd5())
                .setParameter(3, ComputerDataInstance.getComputerData().getIp())
                .setParameter(4, ComputerDataInstance.getComputerData().getComputerName())
                .setParameter(5, ComputerDataInstance.getComputerData().getNameOs())
                .setParameter(6, ComputerDataInstance.getComputerData().getVersionOs())
                .setParameter(7, ComputerDataInstance.getComputerData().getArchitectureOS())
                .setParameter(8, ComputerDataInstance.getComputerData().getAccountName());
        et.begin();
        query.execute();
        et.commit();
        successStatus = (int) query.getOutputParameterValue(9);
        return successStatus == 1 ? (List<UserDetailsEntity>) query.getResultList() : null;
    }

    public static void rollBackIfTransactionIsActive(EntityTransaction entityTransaction) {
        if (entityTransaction.isActive()) {
            entityTransaction.rollback();
        }
    }
}
