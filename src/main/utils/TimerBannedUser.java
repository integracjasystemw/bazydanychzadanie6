package main.utils;

import javafx.application.Platform;
import javafx.scene.control.Label;

import javax.swing.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

/**
 * Created by Mateusz Dobrowolski on 2016-11-11.
 */
public class TimerBannedUser {
    private Timer timer;
    private Label label;
    private int counter = 0;
    private boolean isBanned = false;

    ActionListener tickTimerListener = new ActionListener() {
        @Override
        public void actionPerformed(ActionEvent e) {
            counter++;
            showMessage(String.format("Jesteś jeszcze zbanowany za nadmierne klikanie przez %d sekund", 30 - counter));
            if (counter == 30) {
                isBanned = false;
                timer.stop();
                showMessage("");
            }
        }

        private void showMessage(String message) {
            Platform.runLater(() -> label.setText(message));
        }
    };

    public TimerBannedUser(Label label) {
        this.label = label;
        timer = new Timer(1000, tickTimerListener);
    }

    public void start() {
        isBanned = true;
        counter = 0;
        timer.start();
    }

    public boolean isBanned() {
        return isBanned;
    }
}
