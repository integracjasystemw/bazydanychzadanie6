package main.components.menuitem;

import javafx.collections.FXCollections;
import javafx.event.ActionEvent;
import javafx.event.EventHandler;
import javafx.scene.control.MenuItem;
import main.Main;

/**
 * Created by Mateusz Dobrowolski on 2016-11-04.
 */
public class MenuItemFindUsersByStatus extends MenuItem {
    public MenuItemFindUsersByStatus create() {
        setText("Find user by status");
        setOnAction(new EventHandler<ActionEvent>() {
            @Override
            public void handle(javafx.event.ActionEvent event) {
                Main.controller.setLabelFieldText("Choose status:");
                Main.controller.setOneFieldComboSearching(FXCollections.observableArrayList("Active","Banned"));
                Main.controller.indexMenuItem = 5;
            }
        });
        return this;
    }
}