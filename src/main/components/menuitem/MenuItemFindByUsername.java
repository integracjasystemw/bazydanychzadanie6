package main.components.menuitem;

import javafx.event.ActionEvent;
import javafx.event.EventHandler;
import javafx.scene.control.MenuItem;
import main.Main;

/**
 * Created by Mateusz Dobrowolski on 2016-11-04.
 */
public class MenuItemFindByUsername extends MenuItem {
    public MenuItemFindByUsername create() {
        setText("Find user by username");
        setOnAction(new EventHandler<ActionEvent>() {
            @Override
            public void handle(javafx.event.ActionEvent event) {
                Main.controller.setOneFieldTextSearching();
                Main.controller.setLabelFieldText("Enter username: ");
                Main.controller.indexMenuItem = 0;
            }
        });
        return this;
    }
}