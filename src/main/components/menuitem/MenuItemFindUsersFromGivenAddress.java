package main.components.menuitem;

import javafx.event.ActionEvent;
import javafx.event.EventHandler;
import javafx.scene.control.MenuItem;
import main.Main;

/**
 * Created by Mateusz Dobrowolski on 2016-11-04.
 */
public class MenuItemFindUsersFromGivenAddress extends MenuItem {
    public MenuItemFindUsersFromGivenAddress create() {
        setText("Find users from a given address");
        setOnAction(new EventHandler<ActionEvent>() {
            @Override
            public void handle(javafx.event.ActionEvent event) {
                Main.controller.setTwoFieldTextSearching();
                Main.controller.setLabelFieldText("");
                Main.controller.indexMenuItem = 1;
            }
        });
        return this;
    }
}
