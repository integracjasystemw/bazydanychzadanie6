package main.components.menuitem;

import javafx.event.ActionEvent;
import javafx.event.EventHandler;
import javafx.scene.control.MenuItem;
import main.Main;

/**
 * Created by Mateusz Dobrowolski on 2016-11-04.
 */
public class MenuItemFindUserActiveFromGivenCountOfMonths extends MenuItem {
    public MenuItemFindUserActiveFromGivenCountOfMonths create() {
        setText("Find user active from given count of months");
        setOnAction(new EventHandler<ActionEvent>() {
            @Override
            public void handle(javafx.event.ActionEvent event) {
                Main.controller.setLabelFieldText("Enter count of months:");
                Main.controller.setOneFieldNumericTextSearching();
                Main.controller.indexMenuItem = 3;
            }
        });
        return this;
    }
}
