package main.components.menuitem;

import javafx.collections.FXCollections;
import javafx.event.ActionEvent;
import javafx.event.EventHandler;
import javafx.scene.control.MenuItem;
import main.Main;

/**
 * Created by Mateusz Dobrowolski on 2016-11-04.
 */
public class MenuItemFindUsersByServiceOfWebMail extends MenuItem {
    public MenuItemFindUsersByServiceOfWebMail create() {
        setText("Find users by service of Web Mail");
        setOnAction(new EventHandler<ActionEvent>() {
            @Override
            public void handle(ActionEvent event) {
                Main.controller.setLabelFieldText("Choose service: ");
                Main.controller.setOneFieldComboSearching(FXCollections.observableArrayList("buziaczek.pl", "o2.pl", "onet.pl", "wp.pl", "pollub.edu.pl", "yahoo.com", "gmail.com"));
                Main.controller.indexMenuItem = 8;
            }
        });
        return this;
    }
}
