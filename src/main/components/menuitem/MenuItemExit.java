package main.components.menuitem;


import javafx.application.Platform;
import javafx.event.EventHandler;
import javafx.scene.control.MenuItem;

/**
 * Created by Mateusz Dobrowolski on 2016-10-26.
 */
public class MenuItemExit extends MenuItem {
    public MenuItemExit create() {
        setText("Exit");
        setOnAction(new EventHandler<javafx.event.ActionEvent>() {
            @Override
            public void handle(javafx.event.ActionEvent event) {
                Platform.exit();
            }
        });
        return this;
    }

}
