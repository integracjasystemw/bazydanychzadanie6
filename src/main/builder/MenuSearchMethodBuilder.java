package main.builder;

import javafx.scene.control.Menu;
import main.components.menuitem.*;

/**
 * Created by Mateusz Dobrowolski on 2016-11-02.
 */
public class MenuSearchMethodBuilder extends Menu {
    public Menu create() {
        setText("Search method");
        getItems().addAll(new MenuItemFindByUsername().create(), new MenuItemFindUsersFromGivenAddress().create(), new MenuItemFindAccountsOlderFromGivenOfData().create(), new MenuItemFindUserActiveFromGivenCountOfMonths().create(), new MenuItemFindPersonsByGender().create(), new MenuItemFindUsersByStatus().create(), new MenuItemFindUsersByMobilePhone().create(), new MenuItemFindUserLoggingInGivenPeriodTime().create(), new MenuItemFindUsersByServiceOfWebMail().create(), new MenuItemShowUsersUsedGivenMobileOperator().create());
        return this;
    }
}
