package main.tests;

import com.fasterxml.jackson.dataformat.xml.XmlMapper;
import com.fasterxml.jackson.module.jaxb.JaxbAnnotationModule;
import com.google.gson.Gson;
import main.lists.Results;
import main.presistence.UserDetailsEntity;
import main.utils.JpaProcedureManager;
import net.sourceforge.yamlbeans.YamlException;
import net.sourceforge.yamlbeans.YamlWriter;
import org.junit.Test;

import java.io.File;
import java.io.FileWriter;
import java.io.IOException;
import java.io.Writer;
import java.sql.Date;
import java.util.List;

import static org.junit.Assert.*;
import static org.xmlmatchers.equivalence.IsEquivalentTo.isEquivalentTo;
import static org.xmlmatchers.transform.XmlConverters.the;

/**
 * Created by Mateusz Dobrowolski on 2016-12-04.
 */
public class Tests {

    @Test
    public void testGetUserByUsername() {
        List<UserDetailsEntity> listUsers = JpaProcedureManager.findUser("jenny");
        assertNotNull(listUsers);
        for (UserDetailsEntity userDetailsEntity : listUsers) {
            assertEquals(true, userDetailsEntity.getUsername().contains("jenny"));
        }
    }

    @Test
    public void testGetAccountOlderThan() {
        List<UserDetailsEntity> listUsers = JpaProcedureManager.findAccountsOlderFromDate(Date.valueOf("2013-03-12"));
        assertNotNull(listUsers);
        assertEquals(29, listUsers.size());
    }

    @Test
    public void testUserActiveFromGivenCountOfMonths() {
        List<UserDetailsEntity> listUsers = JpaProcedureManager.findUserActiveFromGivenCountOfMonths("40");
        assertNotNull(listUsers);
        assertEquals(4, listUsers.size());
    }

    @Test
    public void testFindUsersByGender() {
        List<UserDetailsEntity> listUsers = JpaProcedureManager.findUsersByGender("Male");
        assertNotNull(listUsers);
        for (UserDetailsEntity userDetailsEntity : listUsers) {
            assertEquals("Male", userDetailsEntity.getGender());
        }
    }

    @Test
    public void testFindUsersByGender2() {
        List<UserDetailsEntity> listUsers = JpaProcedureManager.findUsersByGender("test");
        assertNotNull(listUsers);
        for (UserDetailsEntity userDetailsEntity : listUsers) {
            assertNotEquals("test", userDetailsEntity.getGender());
        }
    }

    @Test
    public void testFindUsersByServiceOfWebMail() {
        List<UserDetailsEntity> listUsers = JpaProcedureManager.findUsersByServiceOfWebMail("buziaczek.pl");
        assertNotNull(listUsers);
        for (UserDetailsEntity userDetailsEntity : listUsers) {
            assertEquals(true, userDetailsEntity.getEmail().contains("buziaczek.pl"));
        }
    }

    @Test
    public void testFindUsersByMobilePhone() {
        String mobileNumber = "081-451-193";
        List<UserDetailsEntity> listUsers = JpaProcedureManager.findUserByMobilePhoneNumber(mobileNumber);
        assertNotNull(listUsers);
        assertNotEquals(1, listUsers.size());
    }

    @Test
    public void testFindUsersByStatus() {
        List<UserDetailsEntity> listUsers = JpaProcedureManager.findUsersByStatus(0);
        assertNotNull(listUsers);
        assertEquals(0, listUsers.size());
    }

    @Test
    public void testFindUsersByMobilePhone2() {
        List<UserDetailsEntity> listUsers = JpaProcedureManager.findUserByMobilePhoneNumber("857-379-273");
        assertNotNull(listUsers);
        assertEquals(1, listUsers.size());
        assertEquals("081-451-193", listUsers.get(0).getPhoneNumber());
    }

    @Test
    public void testFindUsersFromGivenAddress() {
        String street = "Skalista";
        String city = "Głusk";
        List<UserDetailsEntity> listUsers = JpaProcedureManager.findUsersFromGivenAddress(city, street);
        assertNotNull(listUsers);
        for (UserDetailsEntity userDetailsEntity : listUsers) {
            assertEquals(true, userDetailsEntity.getCity().equals(city));
            assertEquals(true, userDetailsEntity.getStreet().equals(street));
        }
    }

    @Test
    public void testFindUserLogginIngivenPeriodOfTime() {
        List<UserDetailsEntity> listUsers = JpaProcedureManager.findUserLogginIngivenPeriodOfTime(Date.valueOf("2013-02-05"), Date.valueOf("2013-06-13"));
        assertNotNull(listUsers);
        assertEquals(3, listUsers.size());
    }

    @Test
    public void testShowUsersUsedGivenMobileOperator() {
        String operator = "Play";
        List<UserDetailsEntity> listUsers = JpaProcedureManager.showUsersUsedGivenMobileOperator(operator);
        assertNotNull(listUsers);
        for (UserDetailsEntity userDetailsEntity : listUsers) {
            assertTrue(checkMobileOperator(operator, userDetailsEntity.getMobileNumber()));
        }
    }

    @Test
    public void testShowUsersUsedGivenMobileOperator1() {
        String operator = "T-Mobile";
        List<UserDetailsEntity> listUsers = JpaProcedureManager.showUsersUsedGivenMobileOperator(operator);
        assertNotNull(listUsers);
        for (UserDetailsEntity userDetailsEntity : listUsers) {
            assertTrue(checkMobileOperator(operator, userDetailsEntity.getMobileNumber()));
        }
    }


    private boolean checkMobileOperator(String operator, String mobileNumber) {
        switch (operator) {
            case "Orange":
                return mobileNumber.startsWith("50");
            case "Play":
                return mobileNumber.startsWith("7");
            case "Heyah":
                return mobileNumber.startsWith("88");
            case "Plus":
                return mobileNumber.startsWith("60") && (Integer.valueOf(mobileNumber.substring(2, 3)) % 2 == 1) ? true : false;
            case "T-Mobile":
                return mobileNumber.startsWith("60") && (Integer.valueOf(mobileNumber.substring(2, 3)) % 2 == 0) ? true : false;
            default:
                return false;
        }
    }
    @Test
    public void testSaveToJsonFile() throws IOException {
        String json = "{\"userId\":1,\"username\":\"rogers63\",\"firstName\":\"david\",\"lastName\":\"john\",\"gender\":\"Female\",\"city\":\"Lublin\",\"street\":\"Kasztelańska\",\"number\":702,\"email\":\"rogers63@buziaczek.pl\",\"mobileNumber\":\"712-091-036\",\"phoneNumber\":\"081-190-678\"}";
        UserDetailsEntity userDetailsEntity = getUser();
        File file = new File("C:/Users/Braders-Laptop/Desktop/test.json");
        Writer writer = new FileWriter(file.getAbsolutePath());
        Gson gson = new Gson();
        gson.toJson(userDetailsEntity, writer);
        writer.close();
        assertNotNull(file);
        assertEquals(json, gson.toJson(userDetailsEntity));
    }

    public UserDetailsEntity getUser() {
        UserDetailsEntity userDetailsEntity = new UserDetailsEntity();
        userDetailsEntity.setUserId(1);
        userDetailsEntity.setUsername("rogers63");
        userDetailsEntity.setFirstName("david");
        userDetailsEntity.setLastName("john");
        userDetailsEntity.setGender("Female");
        userDetailsEntity.setCity("Lublin");
        userDetailsEntity.setStreet("Kasztelańska");
        userDetailsEntity.setEmail("rogers63@buziaczek.pl");
        userDetailsEntity.setPhoneNumber("081-190-678");
        userDetailsEntity.setMobileNumber("712-091-036");
        userDetailsEntity.setNumber(702);
        return userDetailsEntity;
    }

    @Test
    public void saveToYamlFile() throws YamlException, IOException {
        UserDetailsEntity userDetailsEntity = getUser();
        File file = new File("C:/Users/Braders-Laptop/Desktop/test.yaml");
        YamlWriter writer = new YamlWriter(new FileWriter(file));
        writer.write(userDetailsEntity);
        writer.close();
        assertNotNull(file);
    }

    @Test
    public void saveToXmlFile() throws IOException {
        UserDetailsEntity userDetailsEntity = getUser();
        File file = new File("C:/Users/Braders-Laptop/Desktop/xml.xml");
        XmlMapper xmlMapper = new XmlMapper();
        JaxbAnnotationModule jaxbAnnotationModule = new JaxbAnnotationModule();
        xmlMapper.registerModule(jaxbAnnotationModule);
        xmlMapper.writer().writeValue(file, new Results(userDetailsEntity));
        assertThat(the("<results><user><userId>1</userId><username>rogers63</username><firstName>david</firstName><lastName>john</lastName><gender>Female</gender><city>Lublin</city><street>Kasztelańska</street><number>702</number><email>rogers63@buziaczek.pl</email><mobileNumber>712-091-036</mobileNumber><phoneNumber>081-190-678</phoneNumber></user></results>"),isEquivalentTo(the(xmlMapper.writeValueAsString(new Results((userDetailsEntity))))));
        assertNotNull(file);
    }
}
